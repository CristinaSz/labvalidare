package AngajatiApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class EmployeeTest {
    private Employee e1;
    private Employee e2;
    private Employee e3;
    private Employee e4;
    private Employee e5;
    private Employee e6;
    private Employee e7;
    private Employee e8;
    private Employee e9;
    private Employee e10;

    //se executa automat inaintea fiecarei metode de test.
    @Before
    public void setUp() {
        e1 = new Employee();//"Ionel Pacuraru",1234567890876,ASISTENT,2500,0);
        e1.setFirstName("Ionel");
        e2 = new Employee();//"Mihai Dumitrescu",1244567890876,LECTURER,2500,1);
        e2.setFirstName("Mihai");
        e3 = new Employee();//"Ionela Ionescu",1235567890876,LECTURER,2500,2);
        e3.setFirstName("Ionela");
        e4 = new Employee();//"Mihaela Pacuraru",1234567890876,ASISTENT,2600,3);
        e4.setFirstName("Mihaela");
        e5 = new Employee();//"Vasile Georgescu",1234567890876,TEACHER,2700,4);
        e5.setFirstName("Vasile");
        e6 = new Employee();//"Marin Puscas",1234567890876,TEACHER,2800,5);
        e7 = null; //"Ana Pop",1234567891234,ASISTENT,100.0,6);
        e8 = new Employee();//"Mihai Lupsa",1234567891234,ASISTENT,101.0,7);
        e9 = new Employee();//"Costi Ionica",1234567891234,CONFERENTIAR,99.0,8);
        e10 = null; //"Ana Ionescu",1234567891234,ASISTENT,1000.0,9);

        System.out.println("Before test");
    }

    //se executa automat dupa fiecare metoda de test
    @After
    public void tearDown() {
        e1 = null;
        e2 = null;
        e3 = null;
        e4 = null;
        e5 = null;
        e6 = null;

        System.out.println("After test");
    }

    @Test
    public void getFirstName() {
        assertEquals("Ionel", e1.getFirstName());
    }

    @Test
    public void getFirstName2() {
        assertEquals("Ionel", e1.getFirstName());
    }

    @Test
    public void setFirstName() {
        e1.setFirstName("Mihnea");

        assertEquals("Mihnea", e1.getFirstName());


        System.out.println(e1.getFirstName());
    }

    @Test(expected = NullPointerException.class)
    public void getFirstName4() {
        assertEquals("Mihnea", e10.getFirstName());
    }



    @Test
    public void testConstructor() {
        assertNotEquals("verificam daca s-a creat angajatul 1", e1, null);

        System.out.println(e1.getFirstName());
    }

    @Ignore
    @Test(expected = NullPointerException.class)
    public void testCostructor1() {
        assertEquals("Mihnea", e10.getFirstName());

    }

    @Test(expected = NullPointerException.class)
    public void getFirstName3() {
        assertEquals("Ioana", e10.getFirstName());
    }

    @Test(timeout = 100) //asteapta 10 milisecunde
    public void testFictiv() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeClass
    public static void setUpAll(){
        System.out.println("Before All tests - at the beginning of the Test Class");
    }

    @AfterClass
    public static void tearDownAll(){
        System.out.println("After All tests - at the end of the Test Class");
    }
}