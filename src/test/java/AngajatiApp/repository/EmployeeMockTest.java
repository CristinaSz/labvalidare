package AngajatiApp.repository;

import AngajatiApp.validator.EmployeeException;
import AngajatiApp.validator.EmployeeValidator;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    private EmployeeMock ar;

    @Before
    public void setUp() {
        ar = new EmployeeMock();
    }

    @After
    public void tearDown() {
        ar = null;
    }

    @Test
    public void TC1() {
        Employee e=new Employee();
        e.setId(1);
        e.setFirstName("Harry");
        e.setLastName("Smith");
        e.setCnp("1997865436790");
        e.setFunction(DidacticFunction.ASISTENT);
        e.setSalary(20.6);
        //int nr_angajati=ar.getEmployeeList().size();
        //ar.addEmployee(e);
        //verificam daca intr-adevar angajatul s-a adaugat cu succes
        //assertEquals(nr_angajati+1,ar.getEmployeeList().size());
        assertEquals(true,ar.addEmployee(e));

    }

    @Test
    public void TC2() {
        Employee e=new Employee();
        e.setId(2);
        e.setFirstName("");
        e.setLastName("Marion");
        e.setCnp("1997865436790");
        e.setFunction(DidacticFunction.CONFERENTIAR);
        e.setSalary(25.6);
        assertEquals(false,ar.addEmployee(e));


    }

    @Test
    public void TC3() {
        Employee e=new Employee();
        e.setId(3);
        e.setFirstName("J");
        e.setLastName("Carrey");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(55.6);
        assertEquals(true,ar.addEmployee(e));

    }

//    @Test
//    public void TC7()  {
//        Employee e=new Employee();
//        /*e.setId(3);
//        e.setFirstName("Szakacs");
//        e.setLastName("Cristina");
//        e.setCnp("9876543217654");
//        e.setFunction("doctor");
//        e.setSalary(55.6);*/
//        e.getEmployeeFromString("Cristina;Szakacs;1234567890876;DOCTOR;2600;0", 11);
//        assertEquals(false,ar.addEmployee(e));
//
//
//    }

    @Test
    public void TC4() {
        Employee e=new Employee();
        e.setId(3);
        e.setFirstName("Mi");
        e.setLastName("Smith");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(55.6);
        assertEquals(true,ar.addEmployee(e));

    }
    @Test
    public void TC5() {
        Employee e=new Employee();
        e.setId(3);
        e.setFirstName("MGDAUGDADUGUGSAIG");
        e.setLastName("Irina");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(55.6);
        assertEquals(true,ar.addEmployee(e));

    }

    @Test
    public void TC6() {
        Employee e=new Employee();
        e.setId(3);
        e.setFirstName("MGDAUGDADUGUGSAIGhdgjydgdgdgGAI");
        e.setLastName("INA");
        e.setCnp("9876543217654");
        e.setFunction(DidacticFunction.LECTURER);
        e.setSalary(55.6);
        assertEquals(true,ar.addEmployee(e));

    }

// teste white box

    @Test
    public void modifyEmp1() {   //TC1
        Employee e1=null;
        List<Employee> employeeMockList = ar.getEmployeeList();
        ar.modifyEmployeeFunction(e1,DidacticFunction.ASISTENT);
        assertTrue(ar.getEmployeeList().equals(employeeMockList));
    }

    @Test
    public void modifyEmp2() {   //TC2

        Employee e1 = new Employee();
        e1.setId(1);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        ar.addEmployee(e1);

        List<Employee> employeeMockList = ar.getEmployeeList();
        ar.modifyEmployeeFunction(e1,DidacticFunction.LECTURER);
        assertTrue(e1.getFunction() == DidacticFunction.LECTURER);
    }


    @Test
    public void modifyEmp3() {   //TC3

        Employee e1 = new Employee();
        e1.setId(100);
        e1.setLastName("Daniel");
        e1.setFirstName("Ionut");
        e1.setCnp("1234567891111");
        e1.setFunction(DidacticFunction.ASISTENT);
        e1.setSalary(32.5);
        ar.addEmployee(e1);

        List<Employee> employeeMockList = ar.getEmployeeList();

        ar.modifyEmployeeFunction(e1, DidacticFunction.ASISTENT);
        assertTrue(ar.getEmployeeList().equals(employeeMockList));
    }

    }


